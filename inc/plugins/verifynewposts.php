<?php
if(!defined("IN_MYBB"))
{
    die("This file cannot be accessed directly.");
}

$plugins->add_hook("global_start", "vnp_load_templates");
$plugins->add_hook("showthread_start", "vnp_reply");
$plugins->add_hook("newreply_end", "vnp_reply");
$plugins->add_hook("xmlhttp", "vnp_xmlhttp");

function verifynewposts_info()
{
	return array(
		"name"			=>	"Verify if a thread has newposts when you are replying",
		"description"	=>	"Allows to see if there are new posts inside a thread when you are replying to that thread, and shows by ajax",
		"website"		=>	"https://mybb.com",
		"author"		=>	"Whiteneo",
		"authorsite"	=>	"https://mybb.es",
		"version"		=>	"1.2.0",
		"codename"		=>	"verifynewposts",
		"compatibility" =>	"18*",
	);
}

function verifynewposts_activate()
{
	global $db;

	$templategrouparray = array(
		'prefix' => 'vnp',
		'title'  => 'Verify New Posts'
	);
	$db->insert_query("templategroups", $templategrouparray);
	
	$template1 = array(
		"title"		=> "vnp_template",
		"template"	=> "<div id=\"newpost\"></div>",
		"sid"		=> "-2"
	);
	$db->insert_query("templates", $template1);

	$template2 = array(
		"title"		=> "vnp_template_rows",
		"template"	=> "<div class=\"vnp_newposts post_subject\" id=\"newpost_txt_{\$last_pid}\">
	{\$username} writes a <a href=\"javascript:void(0);\" id=\"show_new_post_{\$last_pid}\" onclick=\"vnp_get_comment({\$last_pid});\">new post inside this thread</a>
	<span class=\"vnp_close\" onclick=\"vnp_remove_comment({\$last_pid});\">[x]</span>
</div>",
		"sid"		=> "-2"
	);
	$db->insert_query("templates", $template2);

	$template3 = array(
		"title"		=> "vnp_template_message",
		"template"	=> "<div class=\"vnp_new_post\">{\$postbit}</div>",
		"sid"		=> "-2"
	);
	$db->insert_query("templates", $template3);

	$template4 = array(
		"title"		=> "vnp_script",
		"template"	=> "<script type=\"text/javascript\">
	var postTimer = 0;
	var curscript = parseInt({\$verify_script});
	var last_pid = parseInt({\$last_pid});
	var curtid = parseInt({\$tid});
	var curname = \'{\$mybb->user[\'username\']}\';
	var lang_newreplies = \'There are new replies on this thread\';
</script>
<script src=\"{\$mybb->settings[\'bburl\']}/jscripts/vnp.js?ver=120\" type=\"text/javascript\"></script>",
		"sid"		=> "-2"
	);
	$db->insert_query("templates", $template4);

	$query = $db->simple_select('themes', 'tid');
	while($theme = $db->fetch_array($query))
	{
		$estilo = array(
			'name'         => 'vnp.css',
			'tid'          => $theme['tid'],
			'attachedto'   => 'showthread.php|newreply.php',
			'stylesheet'   => '.vnp_newposts{background: #c8eeba}.vnp_close{float:right;cursor:pointer;color:#3c6c89}.vnp_new_post{background-color:#DFF2BF}',
			'lastmodified' => TIME_NOW
		);
		$sid = $db->insert_query('themestylesheets', $estilo);
		$db->update_query('themestylesheets', array('cachefile' => "css.php?stylesheet={$sid}"), "sid='{$sid}'", 1);
		require_once MYBB_ADMIN_DIR.'inc/functions_themes.php';
		update_theme_stylesheet_list($theme['tid']);
	}
	
	require MYBB_ROOT.'inc/adminfunctions_templates.php';
    find_replace_templatesets("showthread_quickreply", '#'.preg_quote('<form method="post"').'#', '{$vnp_div}<form method="post"');
    find_replace_templatesets("showthread_quickreply", '#'.preg_quote('</form>').'#', '</form>{$vnp_script}');
    find_replace_templatesets("newreply", '#'.preg_quote('<form action="newreply.php?').'#', '{$vnp_div}<form action="newreply.php?');
    find_replace_templatesets("newreply", '#'.preg_quote('</form>').'#', '</form>{$vnp_script}');	
}

function verifynewposts_deactivate()
{
	global $db;
  	$db->delete_query('themestylesheets', "name IN('vnp.css')");
	$query = $db->simple_select('themes', 'tid');
	while($style = $db->fetch_array($query))
	{
		require_once MYBB_ADMIN_DIR.'inc/functions_themes.php';
		cache_stylesheet($style['tid'], $style['cachefile'], $style['stylesheet']);
		update_theme_stylesheet_list($style['tid'], false, true);	
	}

	$db->delete_query('templategroups', "prefix = 'vnp'");
	$db->delete_query('templates', "title IN('vnp_template','vnp_template_rows','vnp_template_message','vnp_script')");

	require MYBB_ROOT."/inc/adminfunctions_templates.php";
	find_replace_templatesets('showthread_quickreply', '#'.preg_quote('{$vnp_div}').'#', '', 0);
	find_replace_templatesets('showthread_quickreply', '#'.preg_quote('{$vnp_script}').'#', '', 0);
	find_replace_templatesets('newreply', '#'.preg_quote('{$vnp_script}').'#', '', 0);
	find_replace_templatesets('newreply', '#'.preg_quote('{$vnp_div}').'#', '', 0);
}

function vnp_load_templates()
{
	global $mybb, $GLOBALS;
	if($mybb->settings['verifynewposts_active'] == 0)
	{
		return false;
	}
	if(isset($GLOBALS['templatelist']))
	{
		if(THIS_SCRIPT == "showthread.php" || THIS_SCRIPT == "newreply.php")
		{	
			$GLOBALS['templatelist'] .= ',vnp_template,vnp_script';
		}
	}
}

function vnp_reply()
{
	global $mybb, $db, $tid, $vnp_input, $vnp_script, $vnp_div, $templates;
	$query = $db->simple_select("posts", "pid", "tid='{$tid}'", array("order_by" => "pid", "order_dir" => "desc", "limit" => 1));
	$last_pid = (int)$db->fetch_field($query, "pid");
	if(THIS_SCRIPT == "showthread.php")
	{
		$verify_script = 1;
	}
	else if(THIS_SCRIPT == "newreply.php")
	{
		$verify_script = 2;
	}

	eval("\$vnp_script = \"".$templates->get("vnp_script")."\";");
	eval("\$vnp_div = \"".$templates->get("vnp_template")."\";");
}

function vnp_xmlhttp()
{
	global $mybb;
	if($mybb->input['action'] == "get_new_posts")
	{
		$tid = (int)$mybb->input['tid'];
		$last_pid = (int)$mybb->input['last_post'];
		if($tid > 0 && $last_pid > 0)
		{
			global $db, $templates;
			$query = $db->simple_select("posts", "uid, pid, username", "tid='{$tid}' and pid > '{$last_pid}'", array("order_by" => "pid", "order_dir" => "desc", "limit" => 5));
			$template = $username = $errors = "";
			$data = array();
			while($record = $db->fetch_array($query))
			{
				$last_pid = (int)$record['pid'];		
				$username = htmlspecialchars_uni($record['username']);
				$errors = "none";
				eval("\$template .= \"".$templates->get("vnp_template_rows")."\";");
			}
			if($errors == "none")
			{
				$template .= "<script type=\"text/javascript\" src=\"{$mybb->settings['bburl']}/jscripts/vnp_comment.js?ver=110\"></script>";
			}
			$data = array(
				"template" => $template,
				"last_pid" => (int)$last_pid,
				"uname" => $username,
				"error" => $errors
			);
			header("Content-type: application/json; charset=UTF-8");
			echo json_encode($data);
			exit();
		}
	}
	else if($mybb->input['action'] == "show_new_posts")
	{
		$pid = (int)$mybb->input['pid'];
		if($pid > 0)
		{
			global $templates, $forum;
			$post = get_post($pid);
			$forum = get_forum($post['fid']);
			$username = htmlspecialchars_uni($post['username']);
			$postbit = vnp_build_postbit($post, 0);
			eval("\$template = \"".$templates->get("vnp_template_message")."\";");
			$data = array(
				"template" => $template,
				"last_pid" => (int)$pid,
				"uname" => $username
			);
		}
		header("Content-type: application/json; charset=utf-8");
		echo json_encode($data);
		exit();
	}
}

function vnp_build_postbit($post, $post_type = 0)
{
	$user = get_user($post['uid']);
	$post['userusername'] = $post['username'];
	$post = array_merge($post, $user);
	if(!function_exists('build_postbit'))
	{
		include MYBB_ROOT . "inc/functions_post.php";
	}

	$postbit = build_postbit($post, $post_type);
	
	return $postbit;
}
